# sem-proj-tmpl

to be used as an easy starting point for all the semestral project reports.  
effortlessly setup latex environment defined as a `nix` flake, continuously
build && preview.

heavily inspired by [nerdy pepper's one](https://git.peppe.rs/templates/report).

### License
CC0
