DOCNAME=sem-proj
CMD="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode"

.PHONY: $(DOCNAME).pdf all clean

all: $(DOCNAME).pdf

$(DOCNAME).pdf: $(DOCNAME).tex
	latexmk -pdf -pdflatex=$(CMD) -use-make $(DOCNAME).tex

watch: $(DOCNAME).tex
	latexmk -pvc -pdf -pdflatex=$(CMD) -use-make $(DOCNAME).tex

clean:
	latexmk -CA

install:
	cp $(DOCNAME).pdf ${out}/

